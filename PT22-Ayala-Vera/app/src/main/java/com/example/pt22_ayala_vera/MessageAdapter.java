package com.example.pt22_ayala_vera;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.BlocViewHolder> {

    private static int MSG_LEFT = 1;
    private static int MSG_RIGHT = 2;

    private ArrayList<Message> messages;
    private String usuario;
    private Bitmap imagen;

    public MessageAdapter(ArrayList messages, String usuario, Bitmap imagen) {
        this.messages = messages;
        this.usuario = usuario;
        this.imagen = imagen;
    }

    @Override
    public BlocViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == MSG_LEFT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.messageleft, parent, false);
            return new BlocViewHolder(row);
        }
        else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.messageright, parent, false);
            return new BlocViewHolder(row);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(messages.get(position).getUser().equals(usuario)) {
            return MSG_RIGHT;
        }
        else {
            return MSG_LEFT;
        }
    }

    @Override
    public void onBindViewHolder(BlocViewHolder holder, int position) {
        Message bloque = messages.get(position);

        //Insertamos los datos para que se muestren por pantalla
        holder.getTxtViewUsuari().setText(bloque.getUser());

        if(!holder.getImgViewFoto().equals("")) {
            holder.getImgViewFoto().setImageBitmap(this.imagen);
        }
        //else {
            holder.getTxtViewMessege().setText(bloque.getText());
//        }


    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class BlocViewHolder extends RecyclerView.ViewHolder {

        private TextView txtViewUsuari;
        private TextView txtViewMessege;
        private ImageView imgViewFoto;

        public BlocViewHolder(View itemView) {
            super(itemView);

            this.txtViewUsuari = itemView.findViewById(R.id.txtViewUsuari);
            this.txtViewMessege = itemView.findViewById(R.id.txtViewMessage);
            this.imgViewFoto = itemView.findViewById(R.id.imgViewFoto);
        }

        public TextView getTxtViewMessege() {
            return txtViewMessege;
        }

        public TextView getTxtViewUsuari() {
            return txtViewUsuari;
        }

        public ImageView getImgViewFoto() {
            return imgViewFoto;
        }

        public void setTxtViewMessege(TextView txtViewMessege) {
            this.txtViewMessege = txtViewMessege;
        }

        public void setTxtViewUsuari(TextView txtViewUsuari) {
            this.txtViewUsuari = txtViewUsuari;
        }

        public void setImgViewFoto(ImageView imgViewFoto) {
            this.imgViewFoto = imgViewFoto;
        }
    }

    public void add(int position, Message item) {
        messages.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        messages.remove(position);
        notifyItemRemoved(position);
    }

}
