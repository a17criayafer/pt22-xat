package com.example.pt22_ayala_vera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickOpenChat(View view) {

        Intent intent = new Intent(this, ChatActivity.class);
        EditText usuari1 = (EditText) findViewById(R.id.etUsuari1);

        intent.putExtra("usuaris", usuari1.getText().toString());
        startActivity(intent);

    }
}
