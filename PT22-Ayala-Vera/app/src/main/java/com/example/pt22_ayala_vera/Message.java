package com.example.pt22_ayala_vera;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

public class Message {

    public String user;
    public String text;
    public String foto_path;

    public Message() {}

    public Message(String user, String text, String foto_path) {

        this.user = user;
        this.text = text;
        this.foto_path = foto_path;

    }

    //Setters
    public void setUser(String user) {
        this.user = user;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setFoto_path(String foto_path) {
        this.foto_path = foto_path;
    }

    //Getters
    public String getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public String getFoto_path() {
        return foto_path;
    }

    //toString
    @NonNull
    @Override
    public String toString() {
        return super.toString();
    }
}
