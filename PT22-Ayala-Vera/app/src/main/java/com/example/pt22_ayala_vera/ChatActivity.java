package com.example.pt22_ayala_vera;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

public class ChatActivity extends AppCompatActivity {

    private SoundPool soundPool;
    private int soundID;
    private boolean loaded = false;

    private String photoPathDate;

    private String nomUsuari;
    private final ArrayList<Message> messages = new ArrayList<Message>();
    private RecyclerView.LayoutManager layoutManager;
    private StorageReference mStorageRef;
    private StorageReference ImagesRef;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mStorageRef = FirebaseStorage.getInstance().getReference();

        Intent intent = getIntent();
        nomUsuari = intent.getStringExtra("usuaris");

        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                if(sampleId == soundID) loaded = true;
            }
        });
        this.soundID = this.soundPool.load(getApplicationContext(), R.raw.nuevomensaje, 1);

    }

    @Override
    protected void onStart() {
        super.onStart();
        
        final RecyclerView rvMessages = (RecyclerView) findViewById(R.id.rvMessages);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("messages");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Message newMessage = dataSnapshot.getValue(Message.class);
                Log.d("FOTOS", "" + newMessage.getUser() + " " + newMessage.getText() + " " + newMessage.getFoto_path());

                if(!newMessage.getUser().equals(nomUsuari)) {
                    if(loaded) {
                        soundPool.play(soundID, 1.0f, 1.0f, 0, 0, 2f);
                    }
                }

                messages.add(newMessage);

                layoutManager = new LinearLayoutManager(ChatActivity.this);
                rvMessages.setLayoutManager(layoutManager);

                //final MessageAdapter adapter = new MessageAdapter(messages, nomUsuari, bitmap);

                if (!newMessage.getFoto_path().equals("")) {
                    StorageReference pathReference = mStorageRef.child("images/" + newMessage.getFoto_path());
                    final long ONE_MEGABYTE = 1024 * 1024;
                    pathReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            // Data for "images/island.jpg" is returns, use this as needed
                            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                            MessageAdapter adapter = new MessageAdapter(messages, nomUsuari, bitmap);

                            rvMessages.setAdapter(adapter);

                            rvMessages.scrollToPosition(rvMessages.getAdapter().getItemCount() - 1);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });
                }

                else {
                    MessageAdapter adapter = new MessageAdapter(messages, nomUsuari, null);

                    rvMessages.setAdapter(adapter);

                    rvMessages.scrollToPosition(rvMessages.getAdapter().getItemCount() - 1);
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });



    /*myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String user = ds.child("user").getValue(String.class);
                    String body = ds.child("text").getValue(String.class);
                    String fotoPath = ds.child("foto_path").getValue(String.class);
                    Log.d("FOTOS", "" + user + " " + body + " " + fotoPath + "meme");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

    }


    public void sendMessage(View view) {
        //Creamos el mensaje a guardar en BD
        final EditText messageET = findViewById(R.id.etMessage);
        String messageBody = messageET.getText().toString();
        final Message missatge = new Message(nomUsuari, messageBody, "");

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("messages");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                myRef.push().setValue(missatge);
                messageET.getText().clear();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void dispatchTakePictureIntent(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {

            Date actualDate = new Date();
            String strDateFormat = "yyyyMMddhhmmss";
            SimpleDateFormat fechaActual = new SimpleDateFormat(strDateFormat);
            photoPathDate = fechaActual.format(actualDate);
            ImagesRef = mStorageRef.child("images/" + photoPathDate);
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageData = baos.toByteArray();

            UploadTask uploadTask = ImagesRef.putBytes(imageData);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    final EditText messageET = findViewById(R.id.etMessage);
                    String messageBody = messageET.getText().toString();
                    final Message missatge = new Message(nomUsuari, "", photoPathDate);

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    final DatabaseReference myRef = database.getReference("messages");
                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            myRef.push().setValue(missatge);
                            messageET.getText().clear();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            });

        }
    }
}
